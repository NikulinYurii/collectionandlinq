﻿using System;

namespace BSA2021_CollectionAndLINQ.Models
{
    public class TeamDTO 
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}