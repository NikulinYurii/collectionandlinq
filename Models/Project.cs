﻿using System;
using System.Collections.Generic;

namespace BSA2021_CollectionAndLINQ.Models
{
    public class Project
    {
        public int Id { get; set; }
        public User Author { get; set; }
        public Team Team { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
        public List<Task> Tasks { get; set; }

        public override string ToString()
        {
            return
                $"Id: {Id}\n" +
                $"Author: {Author.ToString()}\n" +
                $"Team: {Team.ToString()}\n" +
                $"Name: {Name}\n" +
                $"Description: {Description}\n" +
                $"Deadline: {Deadline}\n" +
                $"CreatedAt: {CreatedAt}";
        }
    }
}