﻿using System;
using System.Collections.Generic;

namespace BSA2021_CollectionAndLINQ.Models
{
    public class User
    {
        public int  Id { get; set; }
        public Team? Team { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }
        public List<Task> Tasks { get; set; }
        
        public override string ToString()
        {
            return
                $"Id: {Id}\n" +
                $"TeamId: {Team.ToString()}\n" +
                $"FirstName: {FirstName}\n" +
                $"LastName: {LastName}\n" +
                $"Email: {Email}\n" +
                $"RegisteredAt: {RegisteredAt}\n" +
                $"BirthDay: {BirthDay}";
        }
    }
}