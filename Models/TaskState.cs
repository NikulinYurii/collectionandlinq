﻿namespace BSA2021_CollectionAndLINQ.Models
{
    public enum TaskState
    {
        Story,
        ToDo,
        InProcess,
        ToVerify,
        Done
    }
}