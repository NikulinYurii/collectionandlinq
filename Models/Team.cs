﻿using System;
using System.Collections.Generic;

namespace BSA2021_CollectionAndLINQ.Models
{
    public class Team
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public List<User> Users { get; set; }
        public override string ToString()
        {
            return
                $"Id: {Id}\n" +
                $"Name: {Name}\n" +
                $"CreatedAt: {CreatedAt}";
        }
    }
}