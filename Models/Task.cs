﻿using System;

namespace BSA2021_CollectionAndLINQ.Models
{
    public class Task
    {
        public int Id { get; set; }
        public Project Project { get; set; }
        public User Performer { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public TaskState State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
        
        public override string ToString()
        {
            return
                $"Id: {Id}\n" +
                $"Project: {Project.ToString()}\n" +
                $"Performer: {Performer.ToString()}\n" +
                $"Name: {Name}\n" +
                $"Description: {Description}\n" +
                $"State: {State}\n" +
                $"CreatedAt: {CreatedAt}\n" +
                $"FinishedAt: {FinishedAt}";
        }
    }
}