﻿using System;
using BSA2021_CollectionAndLINQ.Models;
using BSA2021_CollectionAndLINQ.Services;

namespace BSA2021_CollectionAndLINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            CollectionService service = new CollectionService();

            var res = service.Tasks;
            var res1 = service.Projects;
            var res2 = service.Teams;
            var res3 = service.Users;


            var r = service.GetUserTasksCount(20);
            var r1 = service.GetUserTasks_NameLess45(20);
            var r2 = service.GetUserTasks_Finished2021(20);
            var r3 = service.GetUsersAgeTenPlus();
            var r4 = service.GetUsersByNameAscTastNameLenDesc();
            var r5 = service.GetStructureForSixHomeTask(36);
            var r6 = service.GetStructureForSevenHomeTask();

            Console.WriteLine();
        }
    }
}