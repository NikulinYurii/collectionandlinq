﻿using System;
using System.Collections.Generic;
using System.Linq;
using BSA2021_CollectionAndLINQ.Models;
using ThreadTask = System.Threading.Tasks;

namespace BSA2021_CollectionAndLINQ.Services
{
    public class CollectionService
    {
        private static WebApiService _service = new WebApiService();

        public List<Task> Tasks { get; set; }
        public List<User> Users { get; set; }
        public List<Project> Projects { get; set; }
        public List<Team> Teams { get; set; }

        public CollectionService()
        {
            Tasks =
            (
                from t in _service.GetTasks()
                join p in _service.GetProjects() on t.ProjectId equals p.Id
                join user in _service.GetUsers() on t.PerformerId equals user.Id
                join user2 in _service.GetUsers() on p.AuthorId equals user2.Id
                join team in _service.GetTeams() on p.TeamId equals team.Id
                join team2 in _service.GetTeams() on user.TeamId equals team2.Id
                join team3 in _service.GetTeams() on user2.TeamId equals team3.Id
                select new Task()
                {
                    Id = t.Id,
                    CreatedAt = t.CreatedAt,
                    Description = t.Description,
                    FinishedAt = t.FinishedAt,
                    Name = t.Name,
                    Performer = new User()
                    {
                        BirthDay = user.BirthDay,
                        Email = user.Email,
                        FirstName = user.FirstName,
                        Id = user.Id,
                        LastName = user.LastName,
                        RegisteredAt = user.RegisteredAt,
                        Team = new Team()
                        {
                            Id = team2.Id,
                            Name = team2.Name,
                            CreatedAt = team2.CreatedAt
                        }
                    },
                    State = t.State,
                    Project = new Project()
                    {
                        Id = p.Id,
                        Deadline = p.Deadline,
                        Description = p.Description,
                        Name = p.Name,
                        Team = new Team()
                        {
                            Id = team.Id,
                            Name = team.Name,
                            CreatedAt = team.CreatedAt
                        },
                        Author = new User()
                        {
                            BirthDay = user2.BirthDay,
                            Email = user2.Email,
                            FirstName = user2.FirstName,
                            Id = user2.Id,
                            LastName = user2.LastName,
                            RegisteredAt = user2.RegisteredAt,
                            Team = new Team()
                            {
                                Id = team3.Id,
                                Name = team3.Name,
                                CreatedAt = team3.CreatedAt
                            }
                        }
                    }
                }
            ).ToList();

            Users =
            (
                from u in _service.GetUsers()
                join t in _service.GetTeams() on u.TeamId equals t.Id
                select new User()
                {
                    Id = u.Id,
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    BirthDay = u.BirthDay,
                    Email = u.Email,
                    RegisteredAt = u.RegisteredAt,
                    Team = new Team()
                    {
                        Id = t.Id,
                        Name = t.Name,
                        CreatedAt = t.CreatedAt
                    },
                    Tasks = Tasks.Where(x => x.Performer.Id == u.Id).ToList()
                }
            ).ToList();

            Teams =
            (
                from t in _service.GetTeams()
                select new Team()
                {
                    Id = t.Id,
                    Name = t.Name,
                    CreatedAt = t.CreatedAt,
                    Users = Users.Where(x => x.Team.Id == t.Id).ToList()
                }
            ).ToList();

            Projects =
            (
                from p in _service.GetProjects()
                join t in _service.GetTeams() on p.TeamId equals t.Id
                join u in _service.GetUsers() on p.AuthorId equals u.Id
                select new Project()
                {
                    Id = p.Id,
                    Name = p.Name,
                    Description = p.Description,
                    Deadline = p.Deadline,
                    CreatedAt = p.CreatedAt,
                    Team = Teams.First(x => x.Id == p.TeamId),
                    Author = Users.First(x => x.Id == p.AuthorId),
                    Tasks = Tasks.Where(x => x.Project.Id == p.Id).ToList()
                }
            ).ToList();
        }

        //Отримати кількість тасків у проекті конкретного користувача (по id)
        //(словник, де key буде проект, а value кількість тасків).
        public async ThreadTask.Task<Dictionary<Project, int>> GetUserTasksCount(int userId)
        {
            return await ThreadTask.Task.Run(() => Tasks
                .Where(x => x.Performer.Id == userId)
                .GroupBy(x => x.Project)
                .ToDictionary(
                    x => x.Key,
                    x => x.Count()));
        }

        //Отримати список тасків, призначених для конкретного користувача (по id),
        //де name таска <45 символів (колекція з тасків).
        public async ThreadTask.Task<List<Task>> GetUserTasks_NameLess45(int userId)
        {
            return await ThreadTask.Task.Run(() => Tasks
                .Where(t => t.Name.Length < 45 && t.Performer.Id == userId)
                .ToList());
        }

        //Отримати список (id, name) з колекції тасків, які виконані (finished) в поточному (2021)
        //році для конкретного користувача (по id).
        public async ThreadTask.Task<List<(int Id, string? Name)>> GetUserTasks_Finished2021(int userId)
        {
            return await ThreadTask.Task.Run(() => Tasks
                .Where(t => t.FinishedAt.HasValue
                            && t.FinishedAt.Value.Year == 2021
                            //&& t.State == TaskState.Done
                            && t.Performer.Id == userId)
                .Select(t => (t.Id, t.Name))
                .ToList());
        }

        //Отримати список (id, ім'я команди і список користувачів) з колекції команд,
        //учасники яких старші 10 років, відсортованих за датою реєстрації користувача
        //за спаданням, а також згрупованих по командах.
        public async ThreadTask.Task<List<(int Id, string? Name, List<User>)>> GetUsersAgeTenPlus()
        {
            return await ThreadTask.Task.Run(() => Teams
                .Where(t => t.Users
                    .Count(u => (DateTime.Now.Year - u.BirthDay.Year) > 10) == t.Users.Count)
                .GroupBy(t => t)
                .Select(t => (
                    t.Key.Id,
                    t.Key.Name,
                    t.Key.Users.OrderByDescending(u => u.RegisteredAt).ToList()))
                .ToList());
        }

        //Отримати список користувачів за алфавітом first_name (по зростанню) з відсортованими
        //tasks по довжині name (за спаданням).
        public async ThreadTask.Task<List<User>> GetUsersByNameAscTastNameLenDesc()
        {
            return await ThreadTask.Task.Run(() => Users
                .OrderBy(u => u.FirstName)
                .Select(u => new User()
                {
                    Id = u.Id,
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    Email = u.Email,
                    BirthDay = u.BirthDay,
                    RegisteredAt = u.RegisteredAt,
                    Team = u.Team,
                    Tasks = u.Tasks.OrderByDescending(t => t.Name.Length).ToList()
                }).ToList());
        }

        public async ThreadTask.Task<StructForSixHomeTask> GetStructureForSixHomeTask(int userId)
        {
            return await ThreadTask.Task.Run(() => Projects
                .Where(p => p.Author.Id == userId)
                .OrderByDescending(p => p.CreatedAt)
                .Select(x => new StructForSixHomeTask()
                {
                    User = x.Author,
                    LastProject = x,
                    LastProjectTasksCount = x.Tasks.Count,
                    NotDoneTask = x.Tasks
                        .Where(t => t.Performer.Id == userId)
                        .Count(t => t.FinishedAt.HasValue == false),
                    LongestTask = x.Tasks
                        .Where(t => t.Performer.Id == userId)
                        .OrderByDescending(t => t.FinishedAt - t.CreatedAt)
                        .FirstOrDefault()
                }).FirstOrDefault());
        }

        public async ThreadTask.Task<List<StructForSevenHomeTask>> GetStructureForSevenHomeTask()
        {
            return await ThreadTask.Task.Run(() => Projects
                .Select(x => new StructForSevenHomeTask()
                {
                    Project = x,
                    LongestTaskDescription = x.Tasks
                        .OrderByDescending(t => t.Description)
                        .FirstOrDefault(),
                    ShortestTaskName = x.Tasks
                        .OrderBy(t => t.Name.Length)
                        .FirstOrDefault(),
                    UsersInTeam = x
                        .Team
                        .Users
                        .Count()
                }).ToList());
        }

        public struct StructForSixHomeTask
        {
            public User User { get; set; }
            public Project LastProject { get; set; }
            public int LastProjectTasksCount { get; set; }
            public int NotDoneTask { get; set; }
            public Task LongestTask { get; set; }
        }

        public struct StructForSevenHomeTask
        {
            public Project Project { get; set; }
            public Task LongestTaskDescription { get; set; }
            public Task ShortestTaskName { get; set; }
            public int UsersInTeam { get; set; }
        }
    }
}