﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using BSA2021_CollectionAndLINQ.Models;
using Newtonsoft.Json;

namespace BSA2021_CollectionAndLINQ.Services
{
    public class WebApiService
    {
        private HttpClient client = new HttpClient();

        public List<ProjectDTO> GetProjects()
        {
            string url = $"https://localhost:5001/project/get/all";
            
            var response = client.GetAsync(url);
            var responseRes = response.Result.Content.ReadAsStringAsync().Result;

            if (response.Result.StatusCode == HttpStatusCode.OK)
                return JsonConvert.DeserializeObject<List<ProjectDTO>>(responseRes);
            else
                throw new HttpRequestException(response.Result.StatusCode.ToString());
        }
        
        public List<TaskDTO> GetTasks()
        {
            string url = $"https://localhost:5001/task/get/all";
            
            var response = client.GetAsync(url);
            var responseRes = response.Result.Content.ReadAsStringAsync().Result;

            if (response.Result.StatusCode == HttpStatusCode.OK)
                return JsonConvert.DeserializeObject<List<TaskDTO>>(responseRes);
            else
                throw new HttpRequestException(response.Result.StatusCode.ToString());
        }
        
        public List<TeamDTO> GetTeams()
        {
            string url = $"https://localhost:5001/team/get/all";
            
            var response = client.GetAsync(url);
            var responseRes = response.Result.Content.ReadAsStringAsync().Result;

            if (response.Result.StatusCode == HttpStatusCode.OK)
                return JsonConvert.DeserializeObject<List<TeamDTO>>(responseRes);
            else
                throw new HttpRequestException(response.Result.StatusCode.ToString());
        }
        
        public List<UserDTO> GetUsers()
        {
            string url = $"https://localhost:5001/user/get/all";
            
            var response = client.GetAsync(url);
            var responseRes = response.Result.Content.ReadAsStringAsync().Result;

            if (response.Result.StatusCode == HttpStatusCode.OK)
                return JsonConvert.DeserializeObject<List<UserDTO>>(responseRes);
            else
                throw new HttpRequestException(response.Result.StatusCode.ToString());
        }
        
        public ProjectDTO GetProject(int id)
        {
            string url = $"https://localhost:5001/project/get/{id}";
            
            var response = client.GetAsync(url);
            var responseRes = response.Result.Content.ReadAsStringAsync().Result;

            if (response.Result.StatusCode == HttpStatusCode.OK)
                return JsonConvert.DeserializeObject<ProjectDTO>(responseRes);
            else
                throw new HttpRequestException(response.Result.StatusCode.ToString());
        }
        
        public TaskDTO GetTask(int id)
        {
            string url = $"https://localhost:5001/task/get/{id}";
            
            var response = client.GetAsync(url);
            var responseRes = response.Result.Content.ReadAsStringAsync().Result;

            if (response.Result.StatusCode == HttpStatusCode.OK)
                return JsonConvert.DeserializeObject<TaskDTO>(responseRes);
            else
                throw new HttpRequestException(response.Result.StatusCode.ToString());
        }
        
        public TeamDTO GetTeam(int id)
        {
            string url = $"https://localhost:5001/team/get/{id}";
            
            var response = client.GetAsync(url);
            var responseRes = response.Result.Content.ReadAsStringAsync().Result;

            if (response.Result.StatusCode == HttpStatusCode.OK)
                return JsonConvert.DeserializeObject<TeamDTO>(responseRes);
            else
                throw new HttpRequestException(response.Result.StatusCode.ToString());
        }
        
        public UserDTO GetUser(int id)
        {
            string url = $"https://localhost:5001/user/get/{id}";
            
            var response = client.GetAsync(url);
            var responseRes = response.Result.Content.ReadAsStringAsync().Result;

            if (response.Result.StatusCode == HttpStatusCode.OK)
                return JsonConvert.DeserializeObject<UserDTO>(responseRes);
            else
                throw new HttpRequestException(response.Result.StatusCode.ToString());
        }

        public ProjectDTO AddProject(ProjectDTO projectDto)
        {
            var vehicleJson = JsonConvert.SerializeObject(projectDto);
            HttpContent content = new StringContent(vehicleJson, Encoding.UTF8, "application/json");
            var response = client.PostAsync("https://localhost:5001/project/add", content);
            
            if (response.Result.StatusCode == HttpStatusCode.Created)
            {
                var res = response.Result.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<ProjectDTO>(res);
            }
            else
                throw new HttpRequestException(response.Result.StatusCode.ToString());
        }
        
        public TaskDTO AddTask(TaskDTO taskDto)
        {
            var vehicleJson = JsonConvert.SerializeObject(taskDto);
            HttpContent content = new StringContent(vehicleJson, Encoding.UTF8, "application/json");
            var response = client.PostAsync("https://localhost:5001/task/add", content);
            
            if (response.Result.StatusCode == HttpStatusCode.Created)
            {
                var res = response.Result.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<TaskDTO>(res);
            }
            else
                throw new HttpRequestException(response.Result.StatusCode.ToString());
        }
        
        public TeamDTO AddTask(TeamDTO teamDto)
        {
            var vehicleJson = JsonConvert.SerializeObject(teamDto);
            HttpContent content = new StringContent(vehicleJson, Encoding.UTF8, "application/json");
            var response = client.PostAsync("https://localhost:5001/team/add", content);
            
            if (response.Result.StatusCode == HttpStatusCode.Created)
            {
                var res = response.Result.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<TeamDTO>(res);
            }
            else
                throw new HttpRequestException(response.Result.StatusCode.ToString());
        }
        
        public UserDTO AddTask(UserDTO userDto)
        {
            var vehicleJson = JsonConvert.SerializeObject(userDto);
            HttpContent content = new StringContent(vehicleJson, Encoding.UTF8, "application/json");
            var response = client.PostAsync("https://localhost:5001/user/add", content);
            
            if (response.Result.StatusCode == HttpStatusCode.Created)
            {
                var res = response.Result.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<UserDTO>(res);
            }
            else
                throw new HttpRequestException(response.Result.StatusCode.ToString());
        }

        public void DellProject(int id)
        {
            var response = 
                client.DeleteAsync("https://localhost:5001/project/dell/"+id);
            
            if (response.Result.StatusCode == HttpStatusCode.NotFound)
                throw new HttpRequestException(response.Result.StatusCode.ToString());
        }
        
        public void DellTask(int id)
        {
            var response = 
                client.DeleteAsync("https://localhost:5001/task/dell/"+id);
            
            if (response.Result.StatusCode == HttpStatusCode.NotFound)
                throw new HttpRequestException(response.Result.StatusCode.ToString());
        }
        
        public void DellTeam(int id)
        {
            var response = 
                client.DeleteAsync("https://localhost:5001/team/dell/"+id);
            
            if (response.Result.StatusCode == HttpStatusCode.NotFound)
                throw new HttpRequestException(response.Result.StatusCode.ToString());
        }
        
        public void DellUser(int id)
        {
            var response = 
                client.DeleteAsync("https://localhost:5001/user/dell/"+id);
            
            if (response.Result.StatusCode == HttpStatusCode.NotFound)
                throw new HttpRequestException(response.Result.StatusCode.ToString());
        }
        
        public ProjectDTO UpdateProject(ProjectDTO projectDto)
        {
            var vehicleJson = JsonConvert.SerializeObject(projectDto);
            HttpContent content = new StringContent(vehicleJson, Encoding.UTF8, "application/json");
            var response = client.PostAsync($"https://localhost:5001/project/update/{projectDto.Id}", content);
            
            if (response.Result.StatusCode == HttpStatusCode.Created)
            {
                var res = response.Result.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<ProjectDTO>(res);
            }
            else
                throw new HttpRequestException(response.Result.StatusCode.ToString());
        }
        
        public TaskDTO UpdateTask(TaskDTO taskDto)
        {
            var vehicleJson = JsonConvert.SerializeObject(taskDto);
            HttpContent content = new StringContent(vehicleJson, Encoding.UTF8, "application/json");
            var response = client.PostAsync($"https://localhost:5001/task/update/{taskDto.Id}", content);
            
            if (response.Result.StatusCode == HttpStatusCode.Created)
            {
                var res = response.Result.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<TaskDTO>(res);
            }
            else
                throw new HttpRequestException(response.Result.StatusCode.ToString());
        }
        
        public TeamDTO UpdateTeam(TeamDTO teamDto)
        {
            var vehicleJson = JsonConvert.SerializeObject(teamDto);
            HttpContent content = new StringContent(vehicleJson, Encoding.UTF8, "application/json");
            var response = client.PostAsync($"https://localhost:5001/team/update/{teamDto.Id}", content);
            
            if (response.Result.StatusCode == HttpStatusCode.Created)
            {
                var res = response.Result.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<TeamDTO>(res);
            }
            else
                throw new HttpRequestException(response.Result.StatusCode.ToString());
        }
        
        public UserDTO UpdateUser(UserDTO userDto)
        {
            var vehicleJson = JsonConvert.SerializeObject(userDto);
            HttpContent content = new StringContent(vehicleJson, Encoding.UTF8, "application/json");
            var response = client.PostAsync($"https://localhost:5001/user/update/{userDto.Id}", content);
            
            if (response.Result.StatusCode == HttpStatusCode.Created)
            {
                var res = response.Result.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<UserDTO>(res);
            }
            else
                throw new HttpRequestException(response.Result.StatusCode.ToString());
        }
    }
}